'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('pelanggans', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      nama: {
        type: Sequelize.STRING,
        defaultValue: "",
        allowNull: false,
        unique: true
      },
      tanggal_daftar: {
        type: Sequelize.DATE,
        allowNull: false
      },
      umur: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
        allowNull: false
      },
      no_hp: {
        type: Sequelize.STRING,
        defaultValue: "",
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('pelanggans');
  }
};