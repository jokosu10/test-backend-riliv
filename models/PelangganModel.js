'use strict';
const Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  const Pelanggan = sequelize.define('pelanggan', {
    id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    nama: {
      type: Sequelize.STRING,
      defaultValue: "",
      allowNull: false,
      unique: true,
      validate: {
        min: {
          args: 4,
          msg: "Nama Pelanggan must start with a letter, have no spaces, and be at least 3 characters."
        },
        max: {
          args: 40,
          msg: "Nama Pelanggan must start with a letter, have no spaces, and be at less than 40 characters."
        },
        is: {
          args: /^[A-Za-z][A-Za-z0-9-]*\s?[A-Za-z][A-Za-z0-9-]*\s?[A-Za-z][A-Za-z0-9-]+$/gi, // must start with letter and only have letters, numbers, dashes
          msg: "Nama Pelanggan must start with a letter and be 3 - 40 characters."
        },
        notEmpty: {
          msg: "Please input nama pelanggan"
        }
      }
    },
    tanggal_daftar: {
      type: Sequelize.DATEONLY,
      allowNull: false,
      validate: {
        isDate: true,
        notEmpty: {
          msg: "Please input tanggal daftar pelanggan"
        }
      }
    },
    umur: {
      type: Sequelize.INTEGER,
      defaultValue: 0,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: "Please input umur"
        }
      }
    },
    no_hp: {
      type: Sequelize.STRING,
      defaultValue: "",
      allowNull: false,
      validate: {
        notEmpty: {
          msg: "Please input nomer hp"
        }
      }
    },
    createdAt: {
      type: Sequelize.DATE(),
      allowNull: false,
      defaultValue: Sequelize.NOW
    },
    updatedAt: {
      type: Sequelize.DATE(),
      allowNull: false,
      defaultValue: Sequelize.NOW
    }
  });
  Pelanggan.associate = function (models) {
    // associations can be defined here
    Pelanggan.hasMany(models.pesanan, {
      foreignKey: "id_pelanggan",
      onDelete: 'CASCADE'
    });
  };
  return Pelanggan;
};