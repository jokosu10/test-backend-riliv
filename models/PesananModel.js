'use strict';

const Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  const Pesanan = sequelize.define('pesanan', {
    id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    id_pelanggan: {
      type: DataTypes.INTEGER,
      onDelete: 'CASCADE',
      allowNull: false,
      references: {
        model: "pelanggan",
        key: "id",
      }
    },
    tanggal_pesanan: {
      type: Sequelize.DATEONLY,
      allowNull: false,
      validate: {
        isDate: true,
        notEmpty: {
          msg: "Please input tanggal pesanan"
        }
      }
    },
    total_harga: {
      type: Sequelize.INTEGER,
      defaultValue: 0,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: "Please input total harga"
        }
      }
    },
    createdAt: {
      type: Sequelize.DATE(),
      allowNull: false
    },
    updatedAt: {
      type: Sequelize.DATE(),
      allowNull: false
    }
  });
  Pesanan.associate = function (models) {
    // associations can be defined here
    Pesanan.belongsTo(models.pelanggan, {
      foreignKey: "id_pelanggan"
    });
  };
  return Pesanan;
};