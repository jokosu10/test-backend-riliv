const models = require("../models/Index");
const sequelize = require("sequelize");

const createPelanggan = async (req, res, next) => {
	try {
		const pelanggan = await models.pelanggan.create({
			nama: req.body.nama,
			tanggal_daftar: req.body.tanggal_daftar,
			umur: req.body.umur,
			no_hp: req.body.no_hp
		});

		return res.status(200).json({
			message: 'Success add data pelanggan',
			data: pelanggan
		});
	} catch (error) {
		return res.status(500).json({
			message: error.errors[0].message
		});
	}
}

const getAllPelanggan = async (req, res, next) => {
	try {
		const pelanggan = await models.pelanggan.findAll({});
		if (pelanggan.length !== 0) {
			return res.status(200).json({
				message: 'Success get all data pelanggan',
				data: pelanggan
			});
		} else {
			return res.status(404).json({
				message: 'Data pelanggan is empty',
				data: {}
			});
		}
	} catch (error) {
		return res.status(500).json({
			message: error.errors[0].message
		});
	}
}

const deletePelanggan = async (req, res, next) => {
	try {
		const deletedPelanggan = await models.pelanggan.destroy({
			where: { id: req.params.id }
		});

		if (deletedPelanggan) {
			return res.status(200).json({
				message: 'Successfully delete pelanggan',
			});
		} else {
			return res.status(404).json({
				message: 'Pelanggan not found',
			});
		}
	} catch (error) {
		return res.status(500).json({
			message: error
		});
	}
}

const updatePelanggan = async (req, res, next) => {
	try {
		const pelangganId = await models.pelanggan.findOne({
			where: {
				id: req.params.id
			}
		});

		if (!pelangganId || pelangganId === null) {
			return res.status(404).json({
				message: 'Pelanggan not found',
			});
		} else {
			await models.pelanggan.update({
				nama: req.body.nama,
				tanggal_daftar: req.body.tanggal_daftar,
				umur: req.body.umur,
				no_hp: req.body.no_hp
			}, { where: { id: req.params.id } });

			return res.status(200).json({
				message: 'Successfully update data pelanggan'
			});
		}
	} catch (error) {
		return res.status(500).json({
			message: error
		});
	}
}

module.exports = {
	getAllPelanggan,
	createPelanggan,
	deletePelanggan,
	updatePelanggan
}