const models = require("../models/Index");
const sequelize = require("sequelize");

const createPesanan = async (req, res, next) => {
	try {
		const pesanan = await models.pesanan.create({
			id_pelanggan: req.body.id_pelanggan,
			tanggal_pesanan: req.body.tanggal_pesanan,
			total_harga: req.body.total_harga
		});

		return res.status(200).json({
			message: 'Success add data pelanggan',
			data: pesanan
		});
	} catch (error) {
		return res.status(500).json({
			message: error.errors[0].message
		});
	}
}

const getAllPesanan = async (req, res, next) => {
	try {
		const pesanan = await models.pesanan.findAll({});
		if (pesanan.length !== 0) {
			return res.status(200).json({
				message: 'Success get all data pesanan',
				data: pesanan
			});
		} else {
			return res.status(404).json({
				message: 'Data pesanan is empty',
				data: {}
			});
		}
	} catch (error) {
		return res.status(500).json({
			message: error.errors[0].message
		});
	}
}

const deletePesanan = async (req, res, next) => {
	try {
		const deletedPesanan = await models.pesanan.destroy({
			where: { id: req.params.id }
		});

		if (deletedPesanan) {
			return res.status(200).json({
				message: 'Successfully delete pesanan',
			});
		} else {
			return res.status(404).json({
				message: 'Pesanan not found',
			});
		}
	} catch (error) {
		return res.status(500).json({
			message: error
		});
	}
}

const updatePesanan = async (req, res, next) => {
	try {
		const pesananId = await models.pesanan.findOne({
			where: {
				id: req.params.id
			}
		});

		if (!pesananId || pesananId === null) {
			return res.status(404).json({
				message: 'Pesanan not found',
			});
		} else {
			await models.pesanan.update({
				id_pelanggan: req.body.id_pelanggan,
				tanggal_pesanan: req.body.tanggal_pesanan,
				total_harga: req.body.total_harga
			}, { where: { id: req.params.id } });

			return res.status(200).json({
				message: 'Successfully update data pesanan'
			});
		}
	} catch (error) {
		return res.status(500).json({
			message: error
		});
	}
}

const getPesananByPelangganId = async (req, res, next) => {
	try {

		const pelangganId = await models.pelanggan.findOne({
			where: {
				id: req.params.id
			}
		});

		if (!pelangganId || pelangganId === null) {
			return res.status(404).json({
				message: 'Pelanggan not found',
			});
		} else {
			// const pesanan = await models.pesanan.findAll({
			// 	include: [{
			// 		model: models.pelanggan,
			// 		where: { id: req.params.id }
			// 	}]
			// });
			const pesanan = await models.pesanan.findAll({
				where: { id_pelanggan: req.params.id }
			});
			return res.status(200).json({
				message: 'Successfully get data pesanan by pelanggan id',
				data: pesanan
			});
		}
	} catch (error) {
		return res.status(500).json({
			message: error
		});
	}
}

module.exports = {
	createPesanan,
	getAllPesanan,
	deletePesanan,
	updatePesanan,
	getPesananByPelangganId
}