const jwt = require('jsonwebtoken');
const configJWT = require('../configs/jwt');

const generateToken = (req, res, next) => {
	let mockedUsername = 'admin';
	let mockedPassword = 'password';

	if (mockedUsername && mockedPassword) {
		let token = jwt.sign({ username: mockedUsername },
			configJWT.secret,
			{
				expiresIn: '24h' // expires in 24 hours
			}
		);
		// return the JWT token for the future API calls
		res.status(200).json({
			message: 'Generate token successfully',
			token: token,
			expiresIn: '24h' // expires in 24 hours
		});
	} else {
		res.send(500).json({
			message: 'Generate token failed! Please check the request'
		});
	}
}

module.exports = {
	generateToken
}