const express = require("express");
const router = express();

const AuthRoute = require('../controllers/AuthController');

router.post('/api/v1/token', AuthRoute.generateToken);

module.exports = router;