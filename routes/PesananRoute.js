const middleware = require("../middleware/middleware");
const express = require("express");
const router = express();

const PesananRoute = require('../controllers/PesananController');

router.get('/api/v1/pesanan', middleware.checkToken, PesananRoute.getAllPesanan);
router.post('/api/v1/pesanan', middleware.checkToken, PesananRoute.createPesanan);
router.delete('/api/v1/pesanan/:id', middleware.checkToken, PesananRoute.deletePesanan);
router.patch('/api/v1/pesanan/:id', middleware.checkToken, PesananRoute.updatePesanan);
router.get('/api/v1/pesanan/pelanggan/:id', middleware.checkToken, PesananRoute.getPesananByPelangganId);

module.exports = router;