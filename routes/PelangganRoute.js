const middleware = require("../middleware/middleware");
const express = require("express");
const router = express();

const PelangganRoute = require('../controllers/PelangganController');

router.get('/api/v1/pelanggan', middleware.checkToken, PelangganRoute.getAllPelanggan);
router.post('/api/v1/pelanggan', middleware.checkToken, PelangganRoute.createPelanggan);
router.delete('/api/v1/pelanggan/:id', middleware.checkToken, PelangganRoute.deletePelanggan);
router.patch('/api/v1/pelanggan/:id', middleware.checkToken, PelangganRoute.updatePelanggan);

module.exports = router;