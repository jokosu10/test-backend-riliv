let jwt = require('jsonwebtoken');
const configJWT = require('../configs/jwt');

const checkToken = (req, res, next) => {
	let token = req.headers['authorization'];

	if (token) {
		if (token.startsWith('Bearer ')) {
			// Remove Bearer from string
			token = token.slice(7, token.length);
		}

		jwt.verify(token, configJWT.secret, (err, decoded) => {
			if (err) {
				return res.send(404).json({
					message: 'Token is not valid'
				});
			} else {
				req.decoded = decoded;
				next();
			}
		});
	} else {
		return res.send(500).json({
			message: 'Token is not supplied'
		});
	}
};

module.exports = {
	checkToken: checkToken
};