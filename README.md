# Adonis API application

This application for test Software Engineer in Kasir Pintar.

## Prerequisites

Make sure you install this in your workspace.

```bash
1. NodeJS LTS Version (recommended v10.15.x)
2. NPM (recommended v6.9.x)
3. Git (recommended v2.22.x)
4. PostgreSQL (recommended v11.7)
```

## Setup

Step by step for setup this application.

```bash
1. git clone https://gitlab.com/jokosu10/test-backend-riliv.git
2. cd test-backend-riliv
3. npm install
4. cp .env.example .env
5. set variable in .env
```

## Run server

Run the following command to run server for development mode.

```js
npm run dev
```

### Create Database

Run the following command to run create database.

```js
npm run db_development
```

### Migrations

Run the following command to run startup migrations.

```js
npm run table_development
```

### Testing

Run the following command to run testing application.

```js
npm run test
```

### Run EndPoint Api

Open postman / insomia application. Import collection via link `https://www.getpostman.com/collections/09d0c4f3bad01ffe6c74`.

Have fun :)

### Penjelasan tentang microservice API

Pada api kali ini terdapat 2 tabel yang digunakan yaitu tabel `pelanggans` dan tabel `pesanans`. Relasi kedua tabel tersebut adalah `1:N` / `one to many`. Yang artinya 1 pelanggan dapat memiliki banyak pesanan. Kemudian mode `onUpdate` dan `onDelete` pada relasi kedua tabel tersebut adalah `CASCADE` yang artinya apabila data pelanggan `dihapus` maka data pesanan pelanggan yang berelasi pada data pelanggan berdasarkan `id_pelanggan` akan ikut terhapus. Begitu juga apabila ada data pelanggan yang akan `diupdate`. Seluruh endpoint api telah diamankan dengan menggunakan metode token JWT, sehingga apabila ingin melakukan transaksi melalui endpoint API tetapi tidak menyertakan token JWT akan muncul pesan error.